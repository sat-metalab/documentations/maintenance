# maintenance

Scripts and doc about the documentation - for maintenance purposes.

## Decision Tree - Information Architecture

To generate the graph from the .dot file:
    
    dot -Tsvg decisiontree-infoarchitecture.dot -o result.svg
